﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.IntegrationTests;

using System.Net;
using System.Threading.Tasks;
using NUnit.Framework;

internal sealed class WebsiteTests : ThumbnailerTestsBase
{
    [Test]
    public async Task Health_Get_RespondsWith200()
    {
        // Arrange
        using var client = Services.GetHttpClient();

        // Act
        var response = await client.GetAsync("/health");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
    }

    [TestCase("/"), TestCase("/optimize"), TestCase("/thumbnail")]
    public async Task Page_Get_RespondsWith200(string page)
    {
        // Arrange
        using var client = Services.GetHttpClient();

        // Act
        var response = await client.GetAsync(page);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
    }

    [TestCase("/api/v1/optimize"), TestCase("/api/v1/thumbnail")]
    public async Task Page_PostWithoutParameters_RespondsWith400(string page)
    {
        // Arrange
        using var client = Services.GetHttpClient();

        // Act
        var response = await client.PostAsync(page, null);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
    }

    [TestCase("/index.html"), TestCase("/v1/swagger.json"), TestCase("/v2/swagger.json")]
    public async Task Swagger_Get_RespondsWith200(string endpoint)
    {
        // Arrange
        using var client = Services.GetHttpClient();

        // Act
        var response = await client.GetAsync("/swagger" + endpoint);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
    }

    [Test]
    public async Task Version_Get_RespondsWith200()
    {
        // Arrange
        using var client = Services.GetHttpClient();

        // Act
        var response = await client.GetAsync("/version");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
    }
}
