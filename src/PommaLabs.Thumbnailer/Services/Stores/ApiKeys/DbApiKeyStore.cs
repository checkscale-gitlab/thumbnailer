﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Stores.ApiKeys;

using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using NodaTime;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using SqlKata.Execution;

/// <summary>
///   An API key store which looks for keys on a SQL database.
/// </summary>
public sealed class DbApiKeyStore : ApiKeyStoreBase
{
    private readonly IClock _clock;
    private readonly QueryFactory _db;
    private readonly IOptions<DatabaseConfiguration> _dbc;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="securityConfiguration">Security configuration.</param>
    /// <param name="db">Query factory.</param>
    /// <param name="dbc">Database configuration.</param>
    /// <param name="clock">Clock.</param>
    public DbApiKeyStore(
        IOptions<SecurityConfiguration> securityConfiguration,
        QueryFactory db,
        IOptions<DatabaseConfiguration> dbc,
        IClock clock)
        : base(securityConfiguration)
    {
        _db = db;
        _dbc = dbc;
        _clock = clock;
    }

    /// <inheritdoc/>
    public override async Task<ApiKeyCredentials> AddTempApiKeyAsync(CancellationToken cancellationToken)
    {
        var utcNow = _clock.GetCurrentInstant().ToDateTimeOffset();
        var tempApiKey = new
        {
            name = $"TMP#{utcNow.ToUnixTimeSeconds()}",
            value = Guid.NewGuid().ToString("N", CultureInfo.InvariantCulture).ToUpperInvariant(),
            expires_at = utcNow.AddMinutes(15),
            temporary = true
        };

        await _db
            .Query($"{_dbc.Value.SchemaNameWithDot}api_keys")
            .InsertAsync(tempApiKey, cancellationToken: cancellationToken)
            ;

        return new ApiKeyCredentials
        {
            Name = tempApiKey.name,
            Value = tempApiKey.value,
            IsValid = true
        };
    }

    /// <inheritdoc/>
    public override async Task<int> DeleteExpiredTempApiKeysAsync(CancellationToken cancellationToken)
    {
        return await _db
            .Query($"{_dbc.Value.SchemaNameWithDot}api_keys")
            .WhereTrue("temporary")
            .Where("expires_at", "<", _clock.GetCurrentInstant().ToDateTimeOffset())
            .DeleteAsync(cancellationToken: cancellationToken)
            ;
    }

    /// <inheritdoc/>
    public override async Task<ApiKeyCredentials?> ValidateApiKeyAsync(string apiKey, CancellationToken cancellationToken)
    {
        return (await _db
            .Query($"{_dbc.Value.SchemaNameWithDot}api_keys")
            .Where(q => q.WhereNull("expires_at").OrWhere("expires_at", ">=", _clock.GetCurrentInstant().ToDateTimeOffset()))
            .Select($"name as {nameof(ApiKeyCredentials.Name)}")
            .Select($"value as {nameof(ApiKeyCredentials.Value)}")
            .SelectRaw($"1 as {nameof(ApiKeyCredentials.IsValid)}")
            .GetAsync<ApiKeyCredentials>(cancellationToken: cancellationToken)
            )
            .FirstOrDefault(x => ApiKeysMatch(x.Value, apiKey));
    }
}
