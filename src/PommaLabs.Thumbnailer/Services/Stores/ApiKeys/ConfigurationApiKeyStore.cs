﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Stores.ApiKeys;

using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using NodaTime;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   An API key store which looks for keys on the application configuration file.
/// </summary>
public sealed class ConfigurationApiKeyStore : ApiKeyStoreBase
{
    private readonly IApiKeyStore _apiKeyStore;
    private readonly IClock _clock;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="securityConfiguration">Security configuration.</param>
    /// <param name="apiKeyStore">API key store.</param>
    /// <param name="clock">Clock.</param>
    public ConfigurationApiKeyStore(
        IOptions<SecurityConfiguration> securityConfiguration,
        IApiKeyStore apiKeyStore,
        IClock clock)
        : base(securityConfiguration)
    {
        _apiKeyStore = apiKeyStore;
        _clock = clock;
    }

    /// <inheritdoc/>
    public override Task<ApiKeyCredentials> AddTempApiKeyAsync(CancellationToken cancellationToken)
    {
        return _apiKeyStore.AddTempApiKeyAsync(cancellationToken);
    }

    /// <inheritdoc/>
    public override Task<int> DeleteExpiredTempApiKeysAsync(CancellationToken cancellationToken)
    {
        return _apiKeyStore.DeleteExpiredTempApiKeysAsync(cancellationToken);
    }

    /// <inheritdoc/>
    public override async Task<ApiKeyCredentials?> ValidateApiKeyAsync(string apiKey, CancellationToken cancellationToken)
    {
        var matchedApiKey = SecurityConfiguration.Value.ApiKeys
            .Where(x => ApiKeysMatch(x.Value, apiKey) && (x.ExpiresAt == null || x.ExpiresAt >= _clock.GetCurrentInstant().ToDateTimeOffset()))
            .OrderBy(x => x.Name)
            .Select(x => new ApiKeyCredentials { Name = x.Name, Value = x.Value, IsValid = true })
            .FirstOrDefault();

        return matchedApiKey ?? await _apiKeyStore.ValidateApiKeyAsync(apiKey, cancellationToken);
    }
}
