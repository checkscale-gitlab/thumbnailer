﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Background;

using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Services.Managers.Process;

/// <summary>
///   Runs an "unoserver" process in the background and keeps it alive.
/// </summary>
public sealed class RunUnoserverBackgroundService : BackgroundService
{
    private readonly ILogger<RunUnoserverBackgroundService> _logger;
    private readonly IProcessManager _processManager;
    private readonly int _runnerId;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="runnerId">Runner ID.</param>
    /// <param name="logger">Logger.</param>
    /// <param name="processManager">Process manager.</param>
    public RunUnoserverBackgroundService(
        int runnerId,
        ILogger<RunUnoserverBackgroundService> logger,
        IProcessManager processManager)
    {
        _runnerId = runnerId;
        _logger = logger;
        _processManager = processManager;
    }

    /// <inheritdoc/>
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("unoserver process runner with ID {RunnerId} is starting", _runnerId);

        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                // Each runner starts a process on a port which will be used by one job processor only.
                await _processManager.RunProcessAsync(
                    "unoserver", $"--port {Constants.UnoserverPortStart + _runnerId}",
                    setTimeout: false, cancellationToken: stoppingToken);
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, "An error occurred while running unoserver process with ID {RunnerId}", _runnerId);
            }
        }

        _logger.LogInformation("unoserver process runner with ID {RunnerId} is stopping", _runnerId);
    }
}
