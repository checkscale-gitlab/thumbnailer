﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.HealthChecks;

using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using PommaLabs.Thumbnailer.Services.Stores.ApiKeys;

/// <summary>
///   Checks the status of the API key store.
/// </summary>
public sealed class ApiKeyStoreHealthCheck : IHealthCheck
{
    private readonly IApiKeyStore _apiKeyStore;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="apiKeyStore">API key store.</param>
    public ApiKeyStoreHealthCheck(IApiKeyStore apiKeyStore)
    {
        _apiKeyStore = apiKeyStore;
    }

    /// <inheritdoc/>
    public async Task<HealthCheckResult> CheckHealthAsync(
        HealthCheckContext context, CancellationToken cancellationToken = default)
    {
        try
        {
            await _apiKeyStore.ValidateApiKeyAsync(nameof(ApiKeyStoreHealthCheck), cancellationToken);
            return HealthCheckResult.Healthy("API key store successfully validated test API key");
        }
        catch (Exception ex)
        {
            return HealthCheckResult.Unhealthy("API key store threw an exception while validating test API key", exception: ex);
        }
    }
}
