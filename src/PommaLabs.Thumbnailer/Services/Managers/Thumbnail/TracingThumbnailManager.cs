﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Managers.Thumbnail;

using System.Threading;
using System.Threading.Tasks;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   Thumbnail manager which wraps all operations into activities.
/// </summary>
public sealed class TracingThumbnailManager : IThumbnailManager
{
    private readonly IThumbnailManager _thumbnailManager;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="thumbnailManager">Thumbnail manager.</param>
    public TracingThumbnailManager(IThumbnailManager thumbnailManager)
    {
        _thumbnailManager = thumbnailManager;
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> GenerateThumbnailAsync(ThumbnailGenerationCommand command, CancellationToken cancellationToken)
    {
        using var activity = ThumbnailerActivitySource.Instance.StartActivity(Constants.ActivityNames.ThumbnailGeneration);

        if (activity != null)
        {
            activity.AddTag(Constants.TagNames.CommandInternalJobId, command.InternalJobId);
            activity.AddTag(Constants.TagNames.CommandFileContentType, command.File.ContentType);
        }

        return await _thumbnailManager.GenerateThumbnailAsync(command, cancellationToken);
    }
}
