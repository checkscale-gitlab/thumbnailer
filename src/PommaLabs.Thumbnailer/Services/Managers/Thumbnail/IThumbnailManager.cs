﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Managers.Thumbnail;

using System.Threading;
using System.Threading.Tasks;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   File thumbnail generation manager.
/// </summary>
public interface IThumbnailManager
{
    /// <summary>
    ///   Generates a thumbnail for given file, according to given parameters.
    /// </summary>
    /// <param name="command">Thumbnail generation command.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A thumbnail for given file.</returns>
    Task<TempFileMetadata> GenerateThumbnailAsync(ThumbnailGenerationCommand command, CancellationToken cancellationToken);
}
