﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.Exceptions;

using System;
using System.Runtime.Serialization;

/// <summary>
///   Represents an error produced by a low level process.
/// </summary>
[Serializable]
public class ProcessException : Exception
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="message">Message.</param>
    /// <param name="processName">Process name.</param>
    /// <param name="exitCode">Process exit code.</param>
    public ProcessException(
        string message, string processName, int? exitCode)
        : base(message)
    {
        Data[nameof(ProcessName)] = processName;
        Data[nameof(ExitCode)] = exitCode;
    }

    /// <summary>
    ///   Constructor.
    /// </summary>
    public ProcessException()
    {
    }

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="message">Message</param>
    public ProcessException(string message)
        : base(message)
    {
    }

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="message">Message.</param>
    /// <param name="innerException">Inner exception.</param>
    public ProcessException(string message, Exception innerException)
        : base(message, innerException)
    {
    }

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="info">Serialization info.</param>
    /// <param name="context">Streaming context.</param>
    protected ProcessException(SerializationInfo info, StreamingContext context)
        : base(info, context)
    {
        info.AddValue(nameof(ExitCode), ExitCode);
    }

    /// <summary>
    ///   Process exit code.
    /// </summary>
    public int? ExitCode => Data[nameof(ExitCode)] as int?;

    /// <summary>
    ///   Process name.
    /// </summary>
    public string? ProcessName => Data[nameof(ProcessName)] as string;
}
