﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.Commands;

using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   Represents all information required to perform a media optimization command.
/// </summary>
public sealed class MediaOptimizationCommand : ThumbnailerCommandBase
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="file">File.</param>
    public MediaOptimizationCommand(TempFileMetadata file)
        : base(file)
    {
    }

    /// <inheritdoc/>
    internal override void Validate(Validator validator)
    {
        validator.ValidateContentTypeForMediaOptimization(File.ContentType, @throw: true);
    }
}
