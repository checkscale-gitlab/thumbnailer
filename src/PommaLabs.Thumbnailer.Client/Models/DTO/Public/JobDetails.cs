﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Client.Models.DTO.Public;

using PommaLabs.Thumbnailer.Client.Models.Enumerations;

/// <summary>
///   Represents a queued job whose status can be queried via API.
/// </summary>
public sealed class JobDetails
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="jobId">Job ID.</param>
    public JobDetails(string jobId)
    {
        JobId = jobId;
    }

    /// <summary>
    ///   Failure reason, available when <see cref="Status"/> is equal to <see cref="JobStatus.Failed"/>.
    /// </summary>
    public string? FailureReason { get; set; }

    /// <summary>
    ///   Job ID.
    /// </summary>
    public string JobId { get; }

    /// <summary>
    ///   Status.
    /// </summary>
    public JobStatus Status { get; set; }
}
